# LFP

This is a fork of [lf](https://github.com/gokcehan/lf), please see that [documentation](https://github.com/gokcehan/lf/wiki); I will only document the differences here.

## Changes

- Previews:
  - The previewer script is now given extra arguments in the following order: `height width x y`, lfp-ID; the `x` and `y` arguments are cell coordinates to the top left cell of the previewer pane. This enables true image previews through something like [ueberzug](https://github.com/seebye/ueberzug) or proper sizing with something like [chafa](https://github.com/hpjansson/chafa).
  - New option cleaner runs a script the first time a directory is previewed after a regular file is previewed. This is intended so previewers that make use of ueberzug (or similar) have a chance to clear the screen of any previews when necessary.

## Why

This is intended to be my solution to my sporadic availability of free time. The idea is that I will keep this open as long as necessary for new features, testing ideas, et. al. (sort of similar to i3/i3-gaps), so it is more of a test bench than a hard fork.

Due to this, it is not intended to be the most stable but a better working environment for me (and any others that wish to join) for developing, testing and then submitting new features into lf. However, I cannot see myself actively adding features long term so eventually I may change focus to cleaning the code and offering patches upstream. Additionally, I will work on whichever change catches my fancy at the time so do not expect a specified finish date for anything.

If you have any features that probably won't be accepted upstream, you can either open an [issue](https://gitlab.com/Provessor/lfp/-/issues) requesting the feature here or start working on a [pull request](https://gitlab.com/Provessor/lfp/-/merge_requests) -- this is a rather simple program written in a rather simple language so it doesn't take much expertise to start contributing!
